<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Helper as Helper;

class Book extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'book';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'book_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'book_isbn', 'category', 'book_title', 'book_author', 'book_synopsis',
        'book_borrowed', 'book_stock', 'book_photo',
    ];
    
    /**
     * Generate random String IDs.
     *
     * @var Helper
     */
    public static function boot(){
        parent::boot();

        static::creating(function($model){
           $model->book_id = Helper::randString(); 
        });
    }
}
