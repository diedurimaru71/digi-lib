<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Helper as Helper;

class Student extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'student';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'student_nis';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_nis', 'user_id', 'student_name', 'student_pob', 'student_dob',
        'student_gender', 'student_address', 'student_photo',
    ];

}
