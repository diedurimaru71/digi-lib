<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Helper as Helper;

class Borrow extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'borrow';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'borrowing_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'book_title', 'student_id', 'lended_book', 'return_date', 'borrowing_status',
    ];
    
    /**
     * Generate random String IDs.
     *
     * @var Helper
     */
    public static function boot(){
        parent::boot();

        static::creating(function($model){
           $model->borrowing_id = Helper::randString(); 
        });
    }
}
