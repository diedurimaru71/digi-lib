<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Student;
use App\User;
use App\Borrow as Borrow;
use App\Book as Book;

class SeebookController extends Controller
{
    /*
    *
    * Function to request and Process Sign In on Mobile devices
    *
    */
    public function signin(Request $info){
        try {
            $student = Student::where('student_nis', $info->Nis)->first();
            $data = User::where('id', $student->user_id)->where('password', $info->Password)->first();
            return response()->json([

                'message' => 'Login Success!',
                'serve' => $data

            ], 200);

        } catch (Exception $e) {

            return response()->json([

                'message' => 'Fail',
                'serve' => []

            ], 500);   
        }
    }

    /*
    **
    ** Function to give response and display books on Mobile devices
    **
    */
    public function getBook(){
        try {
            $data['buku'] = Book::all('book_title');
            return response()->json([

                'message' => "Fetched Successfully",
                'serve' => $data['buku']

            ], 200);   
        } catch (Exception $th) {
            return response()->json([

                'message' => "System Failing : $th",
                'serve' => []

            ], 500);   
        }
    }

    /*
    **
    ** Function to give logic of borrowing books on Mobile devices
    **
    */
    public function borrowBook(Request $data){
        try {
            $user = User::where('email', $data->email)->first();
            $nis = $user->id;
            $peminjaman = date('Y-m-d');
            $data['book'] = Book::where('book_title', $data->book_title)->first();
            $data['borrow'] = new borrow;
            
            $data['borrow']->book_title = $data->book_title;
            $data['borrow']->student_nis = $nis;
            $data['borrow']->lended_book = '1';
            $data['borrow']->deadline = date('Y-m-d', strtotime($peminjaman . ' +3 day'));
            $data['borrow']->borrowing_status = "Processing";
            $data['book']->book_stock = $data['book']->book_stock - 1;
            $data['book']->save();
            $data['borrow']->save();

            return response()->json([

                'message' => "Borrowed Successfully",
                'serve' => $data

            ], 200);   
        } catch (Exception $th) {
            return response()->json([

                'message' => "System Failing : $th",
                'serve' => []

            ], 500);   
        }        
    }


    public function takeBook($id){
        try {
            $data['borrow'] = Borrow::where('borrowing_id', $id)->first();
            $data['borrow']->borrowing_status = "Taken";
            $data['borrow']->save();

            return response()->json([
                'message' => "Successfully updated",
                'serve' => $data
            ]);
        } catch (Exception $th) {
            return response()->json([

                'message' => "System Failing : $th",
                'serve' => []

            ]);
        }
    }


    /*
    **
    ** Function to give response and display book details on Mobile devices
    **
    */
    public function detailBook($id){
        try {
            $data['dataBuku'] = Book::where('book_id', $id)->first();
            return response()->json([

                'message' => "Fetched Successfully",
                'serve' => $data

            ], 200);   
        } catch (Exception $th) {
            return response()->json([

                'message' => "System Failing : $th",
                'serve' => []

            ], 500);   
        }
    }



    /*
    **
    ** Function to give response and display book details on Mobile devices
    **
    */
    public function getBorrowed(){
        try {
            $nis = Student::where('user_id', Auth::user()->id)->first();
            $nis = $nis->student_nis;
            $data['borrowingData'] = Borrow::where('student_nis', $nis)->get();
            return response()->json([

                'message' => "Fetched Successfully",
                'serve' => $data

            ], 200);   
        } catch (Exception $th) {
            return response()->json([

                'message' => "System Failing : $th",
                'serve' => []

            ], 500);   
        }
    }

    public function return($id){
        $borrow = Borrow::where('borrowing_id', $id)->first();
        $book = Book::where('book_title', $borrow->book_title)->first();
        $stock = $book->book_stock;
        $book->book_stock = $stock + $borrow->lended_book;
        $book->book_borrowed = $book->book_borrowed + $borrow->lended_book;
        $borrow->return_date = date('Y-m-d');
        $borrow->borrowing_status = "Returned";

        if($borrow->deadline < date('Y-m-d')){
            Session::flash('danger','Fine IDR. 5.000,-');
        }
        else{
            Session::flash('success','Returned Successfully');
        }

        $book->save();
        $borrow->save();
        return redirect('/borrow');
    }

}