<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;
use App\Librarian as Librarian;
use App\User as User;


class LibrarianController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $this->middleware('checkrole:admin');
        $data['librarian'] = Librarian::all();
        return view('/librarian/index', $data);
    }

    public function addLibrarian(Request $data){
        $file = $data->file('file');
        $filename = 'lbr-'.time().'_'.$file->getClientOriginalName();
        $destination = 'img/lbr/';
        $user = new User;
        $user->name = $data->name;
        $user->role = "librarian";
        $user->email = $data->email;
        $user->password = bcrypt($data->password);
        $user->save();
        $librarian = new Librarian;
        $librarian->user_id = $user->id;
        $librarian->librarian_name = $data->name;
        $librarian->librarian_gender = $data->gender;
        $librarian->librarian_address = $data->address;
        $librarian->librarian_photo = $filename;
        $librarian->save();
        $file->move($destination, $filename);
        Session::flash('success','New librarian has been added');
        return redirect('/librarian');
    }

    public function edit($id){
        $this->middleware('checkrole:admin');
        $data['librarian'] = Librarian::where('librarian_id', $id)->first();
        return view('librarian/edit', $data);
    }

    public function updateLibrarian(Request $data){
        $librarian = Librarian::where('librarian_id', $data->id)->first();
        $user = User::where('id', $librarian->user_id)->first();
        $user->name = $data->name;
        $librarian->librarian_name = $data->name;
        $librarian->librarian_gender = $data->gender;
        $librarian->librarian_address = $data->address;
        $user->save();
        $librarian->save();
        Session::flash('warning','Librarian data has been updated');
        return redirect('/librarian');
    }

    public function delete($id){
        $librarian = Librarian::where('librarian_id', $id)->first();
        $user = User::where('id', $librarian->user_id);
        $user->delete();
        
        $librarian = Librarian::where('librarian_id', $id);
        $librarian->delete();
        Session::flash('danger','Librarian data has been deleted');
        return redirect('/librarian');        
    }


    public function search(Request $key){
        $data['librarian'] = Librarian::where('librarian_name','like',"%".$key->keyword."%")->get();
        return view('/librarian/index', $data);
    }

}