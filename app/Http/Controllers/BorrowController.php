<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Book;
use App\Borrow;
use App\Student;
use Session;
use Auth;

class BorrowController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['book'] = Book::all();
        $data['borrow'] = Borrow::all()->sortByDesc('deadline');
        return view('/borrow/index', $data);
    }
    
    public function addBorrow(Request $data){
        $book = Book::where('book_title', $data->book)->first();
        $peminjaman = date('Y-m-d');
        
        //Cek buku
        if(!$book){
            Session::flash('danger','Book not found!');
            return redirect('/borrow');            
        }
        //Cek siswa
        if(!Student::where('student_nis', $data->student)->first()){
            Session::flash('danger','Student not found!');
            return redirect('/borrow');            
        }
        //Cek buku
        if($book->book_stock < $data->book_count){
            Session::flash('danger','Insufficience number of Books lended!');
            return redirect('/borrow');            
        }
        
        $borrow = new borrow;
        $borrow->book_title = $data->book;
        $borrow->student_nis = $data->student;
        $borrow->lended_book = $data->book_count;
        // $borrow->deadline = date('Y-m-d', strtotime($peminjaman . ' +3 day'));
        $borrow->deadline = date('Y-m-d', strtotime($peminjaman . $data->duration));
        $borrow->borrowing_status = "Taken";
        
        $book->book_stock = $book->book_stock - $data->book_count;
        $book->save();
        $borrow->save();
        Session::flash('success','New Transaction has been added');
        return redirect('/borrow');
    }
    
    public function edit($id){
        $this->middleware(['checkrole:librarian', 'checkrole:admin']);
        $data['borrow'] = Borrow::where('borrowing_id', $id)->first();
        $data['book'] = Book::where('book_title', '!=', $data['borrow']['book_title'])->get();
        return view('borrow/edit', $data);
    }
    
    public function updateBorrow(Request $data){
        $borrow = Borrow::where('borrowing_id', $data->id)->first();
        $borrow->borrowing_status = $data->status;
        $borrow->save();
        Session::flash('warning','Borrowing data has been updated');
        return redirect('/borrow');
    }
    
    
    public function search(Request $key){
        $data['book'] = Book::all();
        $data['borrow'] = Borrow::where('borrowing_id','like',"%".$key->keyword."%")->get();
        return view('/borrow/index', $data);
    }
    
    public function return($id){
        $borrow = Borrow::where('borrowing_id', $id)->first();
        $book = Book::where('book_title', $borrow->book_title)->first();
        $stock = $book->book_stock;
        $book->book_stock = $stock + $borrow->lended_book;
        $book->book_borrowed = $book->book_borrowed + $borrow->lended_book;
        $borrow->return_date = date('Y-m-d');
        $borrow->borrowing_status = "Returned";

        if($borrow->deadline < date('Y-m-d')){
            // $finecheck = date('Y-m-d') - $borrow->deadline;
            Session::flash('danger','Fine IDR. 5.000,-');
        }
        else{
            Session::flash('success','Returned Successfully');
        }

        $book->save();
        $borrow->save();
        return redirect('/borrow');
    }

}
    