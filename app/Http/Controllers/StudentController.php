<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Student as Student;

use App\User as User;

use Auth;

use Session;

class StudentController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $this->middleware('checkrole:admin');
        $data['student'] = Student::all();
        return view('/student/index', $data);
    }

    public function addStudent(Request $data){
        $file = $data->file('file');
        $filename = 'std-'.time().'_'.$file->getClientOriginalName();
        $destination = 'img/std/';
        $user = new User;
        $user->name = $data->name;
        $user->role = "student";
        $user->email = $data->email;
        $user->password = bcrypt($data->password);
        $user->save();
        $student = new Student;
        $student->student_nis = $data->nis;
        $student->user_id = $user->id;
        $student->student_name = $data->name;
        $student->student_pob = $data->pob;
        $student->student_dob = $data->dob;
        $student->student_gender = $data->gender;
        $student->student_address = $data->address;
        $student->student_photo = $filename;
        $student->save();
        $file->move($destination, $filename);
        Session::flash('success','New Student data has been added');
        return redirect('/student');
    }

    public function edit($nis){
        $this->middleware('checkrole:admin');
        $data['student'] = Student::where('student_nis', $nis)->first();
        return view('student/edit', $data);
    }

    public function updateStudent(Request $data){
        $student = Student::where('student_nis', $data->nis)->first();
        $user = User::where('id', $student->user_id)->first();
        $user->name = $data->name;
        $student->student_nis = $data->nis;        
        $student->student_name = $data->name;
        $student->student_pob = $data->pob;
        $student->student_dob = $data->dob;
        $student->student_gender = $data->gender;

        if($data->file('file') != null){
            $file = $data->file('file');
            $filename = 'std-'.time().'_'.$file->getClientOriginalName();
            $destination = 'img/std/';
            $student->student_photo = $filename;
            $file->move($destination, $filename);
        }

        $user->save();
        $student->save();
        Session::flash('warning','Student data has been updated');
        return redirect('/student');
    }

    public function delete($nis){
        $student = Student::where('student_nis', $nis)->first();
        $user = User::where('id', $student->user_id);
        $user->delete();
        
        $student = Student::where('student_nis', $nis);
        $student->delete();
        Session::flash('danger','Student data has been deleted');
        return redirect('/student');        
    }


    public function search(Request $key){
        $data['student'] = student::where('student_name','like',"%".$key->keyword."%")->get();
        return view('/student/index', $data);
    }

    public function detail($nis){
        $this->middleware('checkrole:admin');
        $data['student'] = Student::where('student_nis', $nis)->first();
        return view('/student/detail', $data);
    }


}
