<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category as Category;

use App\Book as Book;

use Auth;

use Session;

class BooksController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $this->middleware(['checkrole:librarian', 'checkrole:admin']);
        $data['book'] = Book::orderBy('created_at', 'DESC')->paginate(5);
        $data['categories'] = Category::all();
        return view('/book/index', $data);
    }

    public function addBook(Request $data){
        $file = $data->file('file');
        $filename = 'bks-'.time().'_'.$file->getClientOriginalName();
        $destination = 'img/bks/';

        $book = new Book;
        $book->book_isbn = $data->isbn;
        $book->category = $data->category;
        $book->book_title = $data->title;
        $book->book_author = $data->author;
        $book->book_synopsis = $data->synopsis;
        $book->book_stock = $data->stock;
        $book->book_photo = $filename;
        $book->save();
        $file->move($destination, $filename);
        Session::flash('success','New Book has been added');
        return redirect('/books');
    }

    public function edit($id){
        $this->middleware(['checkrole:librarian', 'checkrole:admin']);
        $data['book'] = Book::where('book_id', $id)->first();
        $data['categories'] = Category::all();
        return view('book/edit', $data);
    }

    public function updateBook(Request $data){
        $book = Book::where('book_id', $data->id)->first();
        $book->book_isbn = $data->isbn;
        $book->category = $data->category;
        $book->book_title = $data->title;
        $book->book_author = $data->author;
        $book->book_synopsis = $data->synopsis;
        $book->book_stock = $data->stock;
        if($data->file('file') != null){
            $file = $data->file('file');
            $filename = 'bks-'.time().'_'.$file->getClientOriginalName();
            $destination = 'img/bks/';
            $book->book_photo = $filename;
            $file->move($destination, $filename);
        }
        $book->save();
        Session::flash('warning','Book data has been updated');
        return redirect('/books');
    }

    public function delete($id){   
        $book = Book::where('book_id', $id);
        $book->delete();
        Session::flash('danger','Book data has been deleted');
        return redirect('/books');        
    }


    public function search(Request $key){
        $data['book'] = Book::where('book_title','like',"%".$key->keyword."%")->get();
        $data['categories'] = Category::all();
        return view('/book/index', $data);
    }


    public function detail($id){
        $this->middleware(['checkrole:librarian', 'checkrole:admin']);
        $data['book'] = Book::where('book_id', $id)->first();
        $data['categories'] = Category::all();
        return view('/book/detail', $data);
    }

}
