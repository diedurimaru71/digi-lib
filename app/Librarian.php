<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Helper as Helper;

class Librarian extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'librarian';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'librarian_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'librarian_name', 'librarian_gender', 'librarian_address', 'librarian_photo',
    ];
    
    /**
     * Generate random String IDs.
     *
     * @var Helper
     */
    public static function boot(){
        parent::boot();

        static::creating(function($model){
           $model->librarian_id = Helper::randString(); 
        });
    }
}
