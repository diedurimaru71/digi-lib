<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book', function (Blueprint $table) {
            $table->string('book_id', 6)->primary();
            $table->string('book_isbn');
            $table->string('category')
                ->foreign('category')
                ->references('category')
                ->on('category')
                ->onDelete('Cascade')
                ->onUpdate('Cascade');
            $table->string('book_title', 50);
            $table->string('book_author', 50);
            $table->text('book_synopsis');
            $table->integer('book_borrowed')->default(0);
            $table->integer('book_stock')->default(0);
            $table->text('book_photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book');
    }
}
