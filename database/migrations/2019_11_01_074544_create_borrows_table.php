<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBorrowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrow', function (Blueprint $table) {
            $table->string('borrowing_id', 6)->primary();
            $table->string('book_id', 6)
                ->foreign('book_id')
                ->references('book_id')
                ->on('book')
                ->onDelete('Cascade')
                ->onUpdate('Cascade');
            $table->string('student_id', 6)
                ->foreign('student_id')
                ->references('student_id')
                ->on('student')
                ->onDelete('Cascade')
                ->onUpdate('Cascade');
            $table->integer('lended_book');
            $table->date('return_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrow');
    }
}
