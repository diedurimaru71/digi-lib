<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->string('student_nis', 9)->primary();
            $table->integer('user_id')
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('Cascade')
                ->onUpdate('Cascade');
            $table->string('student_name', 50);
            $table->string('student_pob', 50);
            $table->date('student_dob', 50);
            $table->enum('student_gender', ['Pria', 'Wanita'])->default('Pria');
            $table->text('student_address');
            $table->text('student_photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}
