<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibrariansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('librarian', function (Blueprint $table) {
            $table->string('librarian_id', 6)->primary();
            $table->integer('user_id')
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('Cascade')
                ->onUpdate('Cascade');
            $table->string('librarian_name', 50);
            $table->enum('librarian_gender', ['Pria', 'Wanita'])->default('Pria');
            $table->text('librarian_address');
            $table->text('librarian_photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('librarian');
    }
}
