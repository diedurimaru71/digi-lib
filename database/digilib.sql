-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2020 at 01:45 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digilib`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `book_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_isbn` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` char(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `book_title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_author` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_synopsis` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_borrowed` int(11) NOT NULL DEFAULT '0',
  `book_stock` int(11) NOT NULL DEFAULT '0',
  `book_photo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`book_id`, `book_isbn`, `category`, `book_title`, `book_author`, `book_synopsis`, `book_borrowed`, `book_stock`, `book_photo`, `created_at`, `updated_at`) VALUES
('7ylUMb', '20192102', 'Novel', 'Pulang', 'Tere Liye', 'Bertanya boleh saja. Bebas. Soal aku mau menjawab atau tidak, itu urusan lain.', 0, 300, 'bks-1578619814_106055_f.jpg', '2020-01-09 18:25:08', '2020-01-09 18:30:14'),
('azbvfy', '20192103', 'Novel', 'Pergi', 'Tere Liye', 'Indit', 0, 300, 'bks-1578619556_0_02e0172c-aeae-4843-9092-ed4d448367b9_627_940.png', '2020-01-09 18:25:56', '2020-01-09 18:25:56'),
('CLCOC0', '20192101', 'Novel', 'Nanti Kita Cerita Tentang Hari Ini', 'Marchella FP', 'Berkisah mengenai Angkasa, Aurora, dan Awan, Kakak Beradik yang hidup dalam keluarga yang tampak bahagia. Setelah mengalami kegagalan besar pertamanya, Awan berkenalan dengan cowok eksentrik bernama Kale. Sedikit demi sedikit sikap Awan mulai berubah, dan Awan pun menadapatkan tekanan dari kedua orang tuanya. Hal tersebut mendorong pemberontakan ketiga kakak beradik ini yang menyebabkan terungkapnya rahasia besar dalam keluarga mereka', 0, 300, 'bks-1578544170_109570_f.jpg', '2020-01-08 21:29:30', '2020-01-08 22:43:23'),
('fIt18r', '20192104', 'Novel', 'Komet', 'Tere Liye', 'Petualangan tiga sahabat, Raib (berasal dari klan Bulan, dia dapat menghilang dan berteportasi, mempunyai sifat pemberani dan slalu berhati-hati dalam mengambil tindakan), Selly (berasal dari klan matahari, dapat mengeluarkan petir dari tangannya, agak penakut namun care dengan sahabatnya) dan Ali (berasal dari klan bumi, dapat berubah jadi beruang buas, paling cerdas diantara teman-temannya sekaligus paling sembrono).', 0, 300, 'bks-1578620573_ID_KT2018MTH05KT__w414_hauto.jpg', '2020-01-09 18:42:55', '2020-01-09 18:42:55'),
('NVv8fH', '20192100', 'Novel', 'Bintang', 'Tere Liye', '”Hidup ini petualangan, Seli, hingga kita mengembuskan napas terakhir. Setiap detiknya berharga, apalagi setiap harinya. Setiap tempat yang kita datangi, setiap orang yang kita temui, kita tidak pernah tau siapa dan apa yang akan terjadi berikutnya. Tapi kita bisa melewatinya dengan selalu tulus, berusaha menjadi orang baik. Lewati petualangan itu bersama sahabat, saling percaya, saling membantu. Saat itu terjadi, dunia paralel menjadi terlalu sempit. Masih banyak tempat lain yang bisa dikunjungi.“', 0, 300, 'bks-1578543972_bintang.jpg', '2019-11-05 18:16:58', '2020-01-09 18:18:26');

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE `borrow` (
  `borrowing_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_nis` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lended_book` int(11) NOT NULL,
  `deadline` date NOT NULL,
  `return_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `borrowing_status` enum('Processing','Ready to Take','Taken','Returning Book','Returned') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Processing'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category`, `created_at`, `updated_at`) VALUES
(1, 'Pendidikan Agama Islam', NULL, NULL),
(2, 'Novel', NULL, NULL),
(3, 'Pendidikan Agama Hindu', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `librarian`
--

CREATE TABLE `librarian` (
  `librarian_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `librarian_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `librarian_gender` enum('Male','Female') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Male',
  `librarian_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `librarian_photo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_01_061116_create_students_table', 1),
(4, '2019_11_01_061951_create_librarians_table', 1),
(5, '2019_11_01_071948_create_books_table', 1),
(6, '2019_11_01_074544_create_borrows_table', 1),
(7, '2019_11_01_162912_create_categories_table', 1),
(8, '2019_11_02_104036_alter_to_borrow_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `student_nis` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `student_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_pob` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_dob` date NOT NULL,
  `student_gender` enum('Male','Female') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Male',
  `student_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_photo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_nis`, `user_id`, `student_name`, `student_pob`, `student_dob`, `student_gender`, `student_address`, `student_photo`, `created_at`, `updated_at`) VALUES
('171810510', 11, 'Faizal Ramadhan', 'Bandung', '2002-11-15', 'Male', 'JL. TERUSAN KOPO GG. AWUG BLOK KENARI', 'std-1578535825_Pas Photo 4 x 6.jpg', '2020-01-08 19:10:26', '2020-01-08 19:10:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','student','librarian') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'student',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Zaraa', 'admin', 'amachannoimeiru@jimeiru.dottocom', NULL, '$2y$10$whV80EdIdLm..dKzM/LKCOjlFBpLRY2LO6PcTXMPJ0LcBTtmVdyEm', NULL, '2019-11-03 02:55:20', '2019-11-05 07:08:31'),
(11, 'Faizal Ramadhan', 'student', 'faizalramadhan15@gmail.com', NULL, '$2y$10$C4pgJKHXxJLF/mUSKtBuqe3vt22JAE637CBVNUYshV5MELrAIQkO.', NULL, '2020-01-08 19:10:26', '2020-01-08 19:10:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `borrow`
--
ALTER TABLE `borrow`
  ADD PRIMARY KEY (`borrowing_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `librarian`
--
ALTER TABLE `librarian`
  ADD PRIMARY KEY (`librarian_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`student_nis`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
