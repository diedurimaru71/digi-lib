<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

	<link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">

    <title>Login to Digilib</title>

    <style>
        body{
            font-weight: lighter;
            font-family: 'Raleway';
        }


        .content{
            background-image: url("{{ asset('/img/login.svg')}}");
            background-repeat: no-repeat;
            width: 100%;
            height: 100vh;
            background-attachment: fixed;
        }

        .card{
            position: absolute;
            margin-top: 110px;
            left: 17.5%;
            background: none;
            color: #00ffff;
            border: 0px;
            width: 600px;

        }
    </style>

</head>
<body>   

<div class="content">
<div class="container">
    <div class="row">
        <div class="col-md-12">


<div class="card">
<br>
<div class="card-header mt-2">
        <h5>Login Form</h5>
</div>
<div class="card-body">


        <form method="POST" action="{{ route('login') }}">
        @csrf

            <div class="form-group">
                <label for="email" class="text-md-right">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="password" class="text-md-right">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                    </label>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-outline-info" style="position: absolute; bottom: 15%; right: 3.4%;">
                    {{ __('Login') }}
                </button>

                @if (Route::has('password.request'))
                    <a class="btn btn-link text-info" href="{{ route('password.request') }}">
                        <small  style="position: absolute; bottom: 6%; left: 3.4%;">{{ __('Forgot Your Password?') }}</small>
                    </a>
                @endif

            </div>
        </form>

</div><!-- card body -->
</div><!-- card -->


                </div>
            </div>
        </div>
        </div></div>

    <script src="{{asset('js/jquery.min.js')}}"></script>
	<script src="{{asset('/js/bootstrap.min.js')}}"></script>
</body>
</html>