@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">


                    <h1>Edit data {{$librarian->librarian_name}}</h1>
                    <form action="/librarian/updateLibrarian" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" class="form-control" id="id" name="id" value="{{$librarian->librarian_id}}">


                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name..." value="{{$librarian->librarian_name}}">
                        </div>

                        <div class="form-group">
                            <label for="gender">Gender</label>
                            <select name="gender" id="gender" class="form-control">
                                <option selected disabled value="{{$librarian->librarian_gender}}">{{$librarian->librarian_gender}}</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea name="address" id="address" cols="5" rows="5" class="form-control" placeholder="Address...">{{$librarian->librarian_address}}</textarea>
                        </div>

                    <button type="submit" class="btn btn-primary" style="border-radius:50px; outline: none;">Save</button>
                </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection