@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

				@if ($message = Session::get('success'))
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>
                        {{ $message }}
                    </strong>
                </div>
				@endif

				@if ($message = Session::get('warning'))
				<div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>
                        {{ $message }}
                    </strong>
                </div>
				@endif

				@if ($message = Session::get('danger'))
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>
                        {{ $message }}
                    </strong>
                </div>
				@endif

                    <h1 style="font-weight:bolder;">Librarians</h1>

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal" style="border-radius:50px; color:#fff; background-color; #3dc6e; outline:none;">
                        Add new Librarian
                    </button>
                    <br>
                    <br>
                    <form action="/librarian/search" method="POST">
                    @csrf
                        <div class="input-group" style="background-color: #fff; border-radius:50px;">
                            <input type="text" class="form-control" name="keyword" placeholder="Search by name..." style="border-radius:50px; border: 0px; outline: none;">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-default" type="submit" style="border-radius:50px;">
                                    <i class="lnr lnr-magnifier"></i>
                                </button>
                            </span>
                        </div><!-- /input-group -->
                    </form>
                    <br>
                    <div class="table-responsive">
                    <table class="table table-hover">
                        <center>
                            <tr class="info">
                                <th>Librarian ID</th>
                                <th>User ID</th>
                                <th>Fullname</th>
                                <th>Gender</th>
                                <th>Address</th>
                                <th>Photo</th>
                                <th>Actions</th>
                            </tr>

                            @foreach($librarian as $l)
                            <tr>
                                <td>{{ $l->librarian_id }}</td>
                                <td>{{ $l->user_id }}</td>
                                <td>{{ $l->librarian_name }}</td>
                                <td>{{ $l->librarian_gender }}</td>
                                <td>{{ $l->librarian_address }}</td>
                                <td><img src="{{url('/img/lbr/'.$l->librarian_photo)}}" alt="{{ $l->librarian_photo }}" style="height: 10%;"></td>
                                <td>
                                    <a href="/librarian/edit/{{ $l->librarian_id }}" class="label label-warning">Edit</a> <br><br>
                                    <a href="/librarian/delete/{{ $l->librarian_id }}" class="label label-danger" onclick="var i = confirm('Are you sure want to delete this data?'); if(i === false){return false}">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </center>
                    </table>
                    </div><!-- /table-responsive -->


                </div>
            </div>
        </div>
    </div>
</div>
@endsection



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Librarian Form</h4>
      </div>
      <div class="modal-body">
          <form action="/librarian/addLibrarian" method="POST" enctype="multipart/form-data">
            @csrf
            
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name..." required>
            </div>

            <div class="form-group">
                <label for="gender">Gender</label>
                <select name="gender" id="gender" class="form-control" required>
                    <option selected disabled>Choose gender...</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </div>

            <div class="form-group">
                <label for="address">Address</label>
                <textarea name="address" id="address" cols="5" rows="5" class="form-control" placeholder="Address..."></textarea required>
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email..." required>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password..." required>
            </div>

            <div class="form-group">
                <label for="exampleInputFile">File input...</label>
                <input type="file" id="exampleInputFile" name="file" class="form-control" required>
            </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" style="border-radius:50px; outline: none;">Close</button>
        <button type="submit" class="btn btn-primary" style="border-radius:50px; outline: none;">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

