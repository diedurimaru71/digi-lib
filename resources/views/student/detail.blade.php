@extends('layouts.master')

@section('content')

                                        <style>

                                            .syn{
                                                background-color: #ffeeaa;
                                                display: flex;
                                                flex-direction: row;
                                                width: 800px;
                                            }

                                            .syn-line{
                                                background-color: #5555ff;
                                                display: block;
                                                flex: 1;
                                            }

                                            .syn-ctn{
                                                text-align:justify;
                                                margin: 10px;
                                                display: block;
                                                flex: 100;
                                            }

                                            /* Style the Image Used to Trigger the Modal */
                                            #myImg {
                                                border-radius: 5px;
                                                cursor: pointer;
                                                transition: 0.3s;
                                            }

                                            #myImg:hover {opacity: 0.7;}

                                            /* The Modal (background) */
                                            .modal {
                                                margin-left : 100px;
                                                display: none; /* Hidden by default */
                                                position: fixed; /* Stay in place */
                                                z-index: 1; /* Sit on top */
                                                padding-top: 150px; /* Location of the box */
                                                left: 0;
                                                top: 0;
                                                width: 100%; /* Full width */
                                                height: 100%; /* Full height */
                                                overflow: auto; /* Enable scroll if needed */
                                                background-color: rgb(0,0,0); /* Fallback color */
                                                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
                                            }

                                            /* Modal Content (Image) */
                                            .modal-content {
                                                margin: auto;
                                                display: block;
                                                width: 20%;
                                                max-width: 500px;
                                            }

                                            /* Caption of Modal Image (Image Text) - Same Width as the Image */
                                            #caption {
                                                margin: auto;
                                                font-weight: regular;
                                                display: block;
                                                width: 80%;
                                                max-width: 700px;
                                                text-align: center;
                                                color: #ccc;
                                                padding: 10px 0;
                                                height: 150px;
                                            }

                                            /* Add Animation - Zoom in the Modal */
                                            .modal-content, #caption {
                                                animation-name: zoom;
                                                animation-duration: 0.6s;
                                            }

                                            @keyframes zoom {
                                                from {transform:scale(0)}
                                                to {transform:scale(1)}
                                            }

                                            /* .dtl-itm::after{
                                                content: " :";
                                            } */

                                            .do{
                                                margin-left: 12%;
                                                background-color: #fff;
                                                padding: 5px;
                                                padding-top: 1px;
                                                width: 76%;
                                                border: 2px dashed #ababab;   
                                            }

                                            .float-btn{
                                                margin-left:86.8%;background-color: #f0ad4e;border-radius: 50px;
                                            }

                                            /* The Close Button */
                                            .close {
                                                z-index: 7;
                                                position: absolute;
                                                top: 15%;
                                                right: 38%;
                                                color: #bbb;
                                                font-size: 40px;
                                                font-weight: bold;
                                                transition: 0.3s;
                                            }

                                            .close:hover,
                                            .close:focus {
                                                color: #bbb;
                                                text-decoration: none;
                                                cursor: pointer;
                                            }

                                            /* 100% Image Width on Smaller Screens */
                                            @media only screen and (max-width: 700px){
                                                .modal-content {
                                                    width: 100%;
                                                }
                                            }
                                        </style>

<div class="main">
    <div class="main-content">
    <a href="/student" class="btn btn-danger float-right" style="border-radius:50px; outline: none; margin-left:0;">Back</a>
<br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <center>
                        <div class="bookimg"  id="myImg"  style="width:200px; height: 200px; overflow: hidden; border-radius: 100%; border: 6px solid #fff; box-shadow: 0 0 10px 2px #888;">
                            <img src="{{url('/img/std/'.$student->student_photo)}}" alt="{{ $student->student_photo }}" class="img rounded" style="height:150%;">
                        </div>
                        <br>
                        <small>Click the image to view the full image</small>
                    </center>

                    <h3 class="text-center" style="font-weight:bolder;">"{{$student->student_name}}"</h3>
<br>
                    <center>
                        <div class="syn">
                            <div class="syn-line"></div>
                            <p class="syn-ctn">{{$student->student_address}}</p>
                        </div>
                    </center>
<br>    
                        <div class="do">
                            <h3 class="dotitle col-md-12">Student Overview <small><i class="lnr lnr-question-circle"></i></small></h3>
                            <span class="dtl-itm col-sm-3">Student NIS</span>: {{$student->student_nis}}</span>
                            <br>
                            <span class="dtl-itm col-sm-3">User ID </span><span class="col-3">: {{$student->user_id}}</span>
                            <br>
                            <span class="dtl-itm col-sm-3">Place of Birth </span><span class="col-3">: {{$student->student_pob}}</span>
                            <br>
                            <span class="dtl-itm col-sm-3">Date of Birth </span><span class="col-3">: {{$student->student_dob}}</span>
                            <br>
                            <span class="dtl-itm col-sm-3">Gender </span><span class="col-3">: {{$student->student_gender}}</span>
                            <br>
                            <div class="float-right"><a href="/student/edit/{{$student->student_nis}}" class="btn btn-warning edit-btn float-btn">Edit data</a></div>
</div>
                        <br>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal">
<center>
  <!-- The Close Button -->
  <span id="close" class="close" style="color:#bbb;">&times;</span>

  <!-- Modal Content (The Image) -->
  <!-- <img class="modal-content" id="img01"> -->
  <img src="{{url('/img/std/'.$student->student_photo)}}" alt="{{ $student->student_photo }}" class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption">"{{$student->student_name}}"</div>
  </center>
</div>

<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
//   modalImg.src = this.src;
//   captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}
</script>

@endsection