@extends('layouts.master')

@section('content')

                                        <style>
                                            /* Style the Image Used to Trigger the Modal */
                                            #myImg {
                                                border-radius: 5px;
                                                cursor: pointer;
                                                transition: 0.3s;
                                            }

                                            #myImg:hover {opacity: 0.7;}

                                            /* The Modal (background) */
                                            .modal {
                                                margin-left : 100px;
                                                display: none; /* Hidden by default */
                                                position: fixed; /* Stay in place */
                                                z-index: 1; /* Sit on top */
                                                padding-top: 150px; /* Location of the box */
                                                left: 0;
                                                top: 0;
                                                width: 100%; /* Full width */
                                                height: 100%; /* Full height */
                                                overflow: auto; /* Enable scroll if needed */
                                                background-color: rgb(0,0,0); /* Fallback color */
                                                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
                                            }

                                            /* Modal Content (Image) */
                                            .modal-content {
                                                margin: auto;
                                                display: block;
                                                width: 20%;
                                                max-width: 500px;
                                            }

                                            /* Caption of Modal Image (Image Text) - Same Width as the Image */
                                            #caption {
                                                margin: auto;
                                                font-weight: regular;
                                                display: block;
                                                width: 80%;
                                                max-width: 700px;
                                                text-align: center;
                                                color: #ccc;
                                                padding: 10px 0;
                                                height: 150px;
                                            }

                                            /* Add Animation - Zoom in the Modal */
                                            .modal-content, #caption {
                                                animation-name: zoom;
                                                animation-duration: 0.6s;
                                            }

                                            @keyframes zoom {
                                                from {transform:scale(0)}
                                                to {transform:scale(1)}
                                            }

                                            /* The Close Button */
                                            .close {
                                                z-index: 7;
                                                position: absolute;
                                                top: 15%;
                                                right: 38%;
                                                color: #bbb;
                                                font-size: 40px;
                                                font-weight: bold;
                                                transition: 0.3s;
                                            }

                                            .close:hover,
                                            .close:focus {
                                                color: #bbb;
                                                text-decoration: none;
                                                cursor: pointer;
                                            }

                                            /* 100% Image Width on Smaller Screens */
                                            @media only screen and (max-width: 700px){
                                                .modal-content {
                                                    width: 100%;
                                                }
                                            }
                                        </style>

<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <center>
                        <div class="bookimg"  id="myImg"  style="width:200px; height: 200px; overflow: hidden; border-radius: 100%; border: 6px solid #fff; box-shadow: 0 0 10px 2px #888;">
                            <img src="{{url('/img/bks/'.$book->book_photo)}}" alt="{{ $book->book_photo }}" class="img rounded" style="height:150%; margin-top: -40px;">
                        </div>
                        <br>
                        <small>Click the image to view the full image</small>
                    </center>
                    
                    <h3 class="text-center" style="font-weight:bolder;">"{{$book->book_title}}"</h3>
<br>
                    <form action="/books/updateBook" method="POST" enctype="multipart/form-data">
                        @csrf
                        
                        <input type="hidden" value="{{$book->book_id}}" name="id">

                        <div class="form-group col-md-6">
                            <label for="isbn">ISBN</label>
                            <input value="{{$book->book_isbn}}" type="text" class="form-control" id="isbn" name="isbn" placeholder="isbn...">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="category">Category</label>
                            <select name="category" id="category" class="form-control">
                                <option selected value="{{$book->category}}">{{$book->category}}</option>
                                @foreach($categories as $c)
                                <option value="{{$c->category}}">{{$c->category}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="author">Book author</label>
                            <input type="text" name="author" class="form-control" id="author" placeholder="author..."  value="{{$book->book_author}}">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="stock">Stock</label>
                            <input type="text" name="stock" class="form-control" id="stock" placeholder="stock..."  value="{{$book->book_stock}}">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="title">Book Title</label>
                            <input type="text" name="title" class="form-control" id="title" placeholder="title..." value="{{$book->book_title}}">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="exampleInputFile">Book Cover</label>
                            <input type="file" id="exampleInputFile" name="file" class="form-control" value="{{$book->book_photo}}">
                        </div>


                        <div class="form-group col-md-12">
                            <label for="synopsis">Synopsis</label>
                            <textarea name="synopsis" id="synopsis" cols="5" rows="5" class="form-control" placeholder="synopsis..." value="{{$book->book_synopsis}}">{{$book->book_synopsis}}</textarea>
                        </div>

                        <div class="asd col-md-6">
                            <!-- <button type="submit" class="btn btn-primary" style="border-radius:50px; outline: none;">Save</button> -->
                            <a href="/books" class="btn btn-danger" style="border-radius:50px; outline: none; margin-left:0;">cancel</a>
                            <button type="submit" class="btn btn-primary" style="border-radius:50px; outline: none;">Save</button>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal">
<center>
  <!-- The Close Button -->
  <span id="close" class="close" style="color:#bbb;">&times;</span>

  <!-- Modal Content (The Image) -->
  <!-- <img class="modal-content" id="img01"> -->
  <img src="{{url('/img/bks/'.$book->book_photo)}}" alt="{{ $book->book_photo }}" class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption" style="font-family: 'Angelina Demo', 'Poppins';">"{{$book->book_title}}"</div>
  </center>
</div>

<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
//   modalImg.src = this.src;
//   captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}
</script>

@endsection