@extends('layouts.master')

@section('content')

                                        <style>

                                            .syn{
                                                background-color: #ffeeaa;
                                                display: flex;
                                                flex-direction: row;
                                                width: 800px;
                                            }

                                            .syn-line{
                                                background-color: #3dc06e;
                                                display: block;
                                                flex: 1;
                                            }

                                            .syn-ctn{
                                                text-align:justify;
                                                margin: 10px;
                                                display: block;
                                                flex: 100;
                                            }

                                            /* Style the Image Used to Trigger the Modal */
                                            #myImg {
                                                border-radius: 5px;
                                                cursor: pointer;
                                                transition: 0.3s;
                                            }

                                            #myImg:hover {opacity: 0.7;}

                                            /* The Modal (background) */
                                            .modal {
                                                margin-left : 100px;
                                                display: none; /* Hidden by default */
                                                position: fixed; /* Stay in place */
                                                z-index: 1; /* Sit on top */
                                                padding-top: 150px; /* Location of the box */
                                                left: 0;
                                                top: 0;
                                                width: 100%; /* Full width */
                                                height: 100%; /* Full height */
                                                overflow: auto; /* Enable scroll if needed */
                                                background-color: rgb(0,0,0); /* Fallback color */
                                                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
                                            }

                                            /* Modal Content (Image) */
                                            .modal-content {
                                                margin: auto;
                                                display: block;
                                                width: 20%;
                                                max-width: 500px;
                                            }

                                            /* Caption of Modal Image (Image Text) - Same Width as the Image */
                                            #caption {
                                                margin: auto;
                                                font-weight: regular;
                                                display: block;
                                                width: 80%;
                                                max-width: 700px;
                                                text-align: center;
                                                color: #ccc;
                                                padding: 10px 0;
                                                height: 150px;
                                            }

                                            /* Add Animation - Zoom in the Modal */
                                            .modal-content, #caption {
                                                animation-name: zoom;
                                                animation-duration: 0.6s;
                                            }

                                            @keyframes zoom {
                                                from {transform:scale(0)}
                                                to {transform:scale(1)}
                                            }

                                            /* .dtl-itm::after{
                                                content: " :";
                                            } */

                                            .do{
                                                margin-left: 12%;
                                                background-color: #fff;
                                                padding: 5px;
                                                padding-top: 1px;
                                                width: 76%;
                                                border: 2px dashed #ababab;   
                                            }

                                            .float-btn{
                                                margin-left:86.8%;background-color: #f0ad4e;border-radius: 50px;
                                            }

                                            /* The Close Button */
                                            .close {
                                                z-index: 7;
                                                position: absolute;
                                                top: 15%;
                                                right: 38%;
                                                color: #bbb;
                                                font-size: 40px;
                                                font-weight: bold;
                                                transition: 0.3s;
                                            }

                                            .close:hover,
                                            .close:focus {
                                                color: #bbb;
                                                text-decoration: none;
                                                cursor: pointer;
                                            }

                                            /* 100% Image Width on Smaller Screens */
                                            @media only screen and (max-width: 700px){
                                                .modal-content {
                                                    width: 100%;
                                                }
                                            }
                                        </style>

<div class="main">
    <div class="main-content">
    <a href="/books" class="btn btn-danger float-right" style="border-radius:50px; outline: none; margin-left:0;">Back</a>
<br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <center>
                        <div class="bookimg"  id="myImg"  style="width:200px; height: 200px; overflow: hidden; border-radius: 100%; border: 6px solid #fff; box-shadow: 0 0 10px 2px #888;">
                            <img src="{{url('/img/bks/'.$book->book_photo)}}" alt="{{ $book->book_photo }}" class="img rounded" style="height:150%; margin-top: -40px;">
                        </div>
                        <br>
                        <small>Click the image to view the full resolution</small>
                    </center>
                    
                    <h3 class="text-center" style="font-weight:bolder;">"{{$book->book_title}}"</h3>
<br>
                    <center>
                        <div class="syn">
                            <div class="syn-line"></div>
                            <p class="syn-ctn">{{$book->book_synopsis}}</p>
                        </div>
                    </center>
<br>    
                        <div class="do">
                            <h3 class="dotitle col-md-12">Detail Overview <small><i class="lnr lnr-question-circle"></i></small></h3>
                            <span class="dtl-itm col-sm-3">Book Id</span>: {{$book->book_id}}</span>
                            <br>
                            <span class="dtl-itm col-sm-3">ISBN </span><span class="col-3">: {{$book->book_isbn}}</span>
                            <br>
                            <span class="dtl-itm col-sm-3">Category </span><span>: {{$book->category}}</span>
                            <br>
                            <span class="dtl-itm col-sm-3">Author </span><span>: {{$book->book_author}}</span>
                            <br>
                            <span class="dtl-itm col-sm-3">Borrowed </span><span>: {{$book->book_borrowed}}</span>
                            <br>
                            <span class="dtl-itm col-sm-3">Available Stock </span><span>: {{$book->book_stock}}</span>
                            <br>
                            <div class="float-right"><a href="/books/edit/{{$book->book_id}}" class="btn btn-warning edit-btn float-btn">Edit data</a></div>
                            <!-- <br> -->


                        </div>
                        <br>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal">
<center>
  <!-- The Close Button -->
  <span id="close" class="close" style="color:#bbb;">&times;</span>

  <!-- Modal Content (The Image) -->
  <!-- <img class="modal-content" id="img01"> -->
  <img src="{{url('/img/bks/'.$book->book_photo)}}" alt="{{ $book->book_photo }}" class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption">"{{$book->book_title}}"</div>
  </center>
</div>

<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
//   modalImg.src = this.src;
//   captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}
</script>

@endsection