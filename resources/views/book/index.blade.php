@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

				@if ($message = Session::get('success'))
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>
                        {{ $message }}
                    </strong>
                </div>
				@endif

				@if ($message = Session::get('warning'))
				<div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>
                        {{ $message }}
                    </strong>
                </div>
				@endif

				@if ($message = Session::get('danger'))
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>
                        {{ $message }}
                    </strong>
                </div>
				@endif

                    <h1 style="font-weight:bolder;">Books</h1>

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal" style="border-radius:50px; color:#fff; background-color; #3dc6e; outline:none;">
                        Add new Book
                    </button>
                    <br>
                    <br>
                    <form action="/books/search" method="POST">
                    @csrf
                        <div class="input-group" style="background-color: #fff; border-radius:50px;">
                            <input type="text" class="form-control" name="keyword" placeholder="Search by title..." style="border-radius:50px; border: 0px; outline: none;">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-default" type="submit" style="border-radius:50px; height: 35px;">
                                    <i class="lnr lnr-magnifier" style="font-weight:bold;"></i>
                                </button>
                            </span>
                        </div><!-- /input-group -->
                    </form>
                    <br>
                    <div class="table-responsive">

    {{ $book->links("pagination::bootstrap-4") }}
                    
                    <table class="table table-hover">
                        <center>
                            <tr class="info">
                                <th>Book ID</th>
                                <th>ISBN</th>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Borrowed</th>
                                <th>Stock</th>
                                <th>Photo</th>
                                <th>Actions</th>
                            </tr>

                            @foreach($book as $b)
                            <tr>
                                <td>{{ $b->book_id }}</td>
                                <td>{{ $b->book_isbn }}</td>
                                <!-- <td>{{ $b->category }}</td> -->
                                <td>{{ $b->book_title }}</td>
                                <td>{{ $b->book_author }}</td>
                                <!-- <td style="text-overflow:ellipsis; white-space:nowrap; overflow: hidden; max-width: 250px;">{{ $b->book_synopsis }}</td> -->
                                <td><center>{{ $b->book_borrowed }}</center></td>
                                <td>{{ $b->book_stock }}</td>
                                <!-- <td>{{ $b->book_photo }}</td> -->
                                <td><img src="{{url('/img/bks/'.$b->book_photo)}}" alt="{{ $b->book_photo }}" style="height: 10%;"></td>

                                <td><br>
                                    <a href="/books/detail/{{ $b->book_id }}" class="label label-info">Detail</a> <br><br>
                                    <a href="/books/edit/{{ $b->book_id }}" class="label label-warning">Edit</a> <br><br>
                                    <a href="/books/delete/{{ $b->book_id }}" class="label label-danger" onclick="var i = confirm('Are you sure want to delete this data?'); if(i === false){return false}">Delete</a><br>
                                </td>
                            </tr>
                            @endforeach
                        </center>
                    </table>
                    </div><!-- /table-responsive -->


                </div>
            </div>
        </div>
    </div>
</div>
@endsection



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Book Form</h4>
      </div>
      <div class="modal-body">
          <form action="/books/addBook" method="POST" enctype="multipart/form-data">
            @csrf
            
            <div class="form-group">
                <label for="isbn">ISBN</label>
                <input type="text" class="form-control" id="isbn" name="isbn" placeholder="isbn..." required>
            </div>

            <div class="form-group">
                <label for="category">Category</label>
                <select name="category" id="category" class="form-control" required>
                    <option selected disabled>Choose category...</option>
                    @foreach($categories as $c)
                    <option value="{{$c->category}}">{{$c->category}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="title">Book Title</label>
                <input type="text" name="title" class="form-control" id="title" placeholder="title..." required>
            </div>

            <div class="form-group">
                <label for="author">Book author</label>
                <input type="text" name="author" class="form-control" id="author" placeholder="author..." required>
            </div>

            <div class="form-group">
                <label for="synopsis">Synopsis</label>
                <textarea name="synopsis" id="synopsis" cols="5" rows="5" class="form-control" placeholder="synopsis..."></textarea required>
            </div>

            <div class="form-group">
                <label for="stock">Stock</label>
                <input type="text" name="stock" class="form-control" id="stock" placeholder="stock..." required>
            </div>

            <div class="form-group">
                <label for="exampleInputFile">Book Cover</label>
                <input type="file" id="exampleInputFile" name="file" class="form-control" required>
            </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" style="border-radius:50px; outline: none;">Close</button>
        <button type="submit" class="btn btn-primary" style="border-radius:50px; outline: none;">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

