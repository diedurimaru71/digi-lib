@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- <div class="panel panel-default">
                        <div class="panel-heading"><h4 style="font-weight:bolder;">Home</h4></div>
                        <div class="panel-body">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif

                        You are logged in!
                        </div>
                    </div> -->
                    <center>
                        <img src="{{asset('img/admin-home.svg')}}" alt="" style="height: 60%; opacity:70%;">
                        <h1>Hello, <span style="color: #009a9a;">{{Auth::user()->name}}!</span></h1>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection