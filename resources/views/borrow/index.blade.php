@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

				@if ($message = Session::get('success'))
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>
                        {{ $message }}
                    </strong>
                </div>
				@endif

				@if ($message = Session::get('warning'))
				<div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>
                        {{ $message }}
                    </strong>
                </div>
				@endif

				@if ($message = Session::get('danger'))
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>
                        {{ $message }}
                    </strong>
                </div>
				@endif

                    <h1 style="font-weight:bolder;">Borrowing</h1>

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal" style="border-radius:50px; color:#fff; background-color; #3dc6e; outline:none;">
                        Add new Transaction
                    </button>
                    <br>
                    <br>
                    <form action="/borrow/search" method="POST">
                    @csrf
                        <div class="input-group" style="background-color: #fff; border-radius:50px;">
                            <input type="text" class="form-control" name="keyword" placeholder="Search by ID..." style="border-radius:50px; border: 0px; outline: none;">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-default" type="submit" style="border-radius:50px;">
                                    <i class="lnr lnr-magnifier"></i>
                                </button>
                            </span>
                        </div><!-- /input-group -->
                    </form>
                    <br>
                    <div class="table-responsive">
                    <table class="table table-hover">
                        <center>
                            <tr class="info">
                                <th>Borrowing ID</th>
                                <th>Book Title</th>
                                <th>Student NIS</th>
                                <th>Lended Book</th>
                                <th>Deadline</th>
                                <th>Return Date</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>

                            @foreach($borrow as $b)
                            <tr>
                                <td>{{ $b->borrowing_id }}</td>
                                <td>{{ $b->book_title }}</td>
                                <td>{{ $b->student_nis }}</td>
                                <td>{{ $b->lended_book }}</td>
                                <td>{{ $b->deadline }}</td>
                                <td>{{ $b->return_date }}</td>
                                <td>{{ $b->borrowing_status }}</td>
                                <td>
                                    @if($b->borrowing_status == "Returned")
                                        <code>None</code>
                                    
                                    @else
                                        <a href="/borrow/edit/{{ $b->borrowing_id }}" class="label label-warning">Edit</a> <br> <br>
                                        <a href="/borrow/return/{{ $b->borrowing_id }}" class="label label-success">Return</a>
                                    
                                    @endif

                                </td>
                            </tr>
                            @endforeach
                        </center>
                    </table>
                    </div><!-- /table-responsive -->


                </div>
            </div>
        </div>
    </div>
</div>
@endsection



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Transaction Form</h4>
      </div>
      <div class="modal-body">
          <form action="/borrow/addBorrow" method="POST">
            @csrf
            
            <div class="form-group">
                <label for="student">Student NIS</label>
                <input type="text" class="form-control" id="student" name="student" placeholder="student..." required>
            </div>

            <div class="form-group">
                <label for="book">Book Title</label>
                <select name="book" id="book" class="form-control" required>
                    <option selected disabled>Choose book...</option>
                    @foreach($book as $b)
                        <option value="{{$b->book_title}}">{{$b->book_title}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="book_count">Count</label>
                <input type="number" name="book_count" class="form-control" id="book_count" placeholder="book count..." required>
            </div>

            <div class="form-group">
                <label for="book">Duration</label>
                <select name="duration" id="duration" class="form-control" required>
                    <option selected disabled>Select Duration...</option>
                    <option class="" value=" +1 day">1 Day</option>
                    <option class="" value=" +3 day">3 Day</option>
                    <option class="" value=" +7 day">7 Day</option>
                    <option class="" value=" -1 day">Debug Feature</option>
                </select>
            </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" style="border-radius:50px; outline: none;">Close</button>
        <button type="submit" class="btn btn-primary" style="border-radius:50px; outline: none;">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

