@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">


                    <h1>Update Borrowing Data</h1>
                    <form action="/borrow/updateBorrow" method="POST" enctype="multipart/form-data">
                        @csrf
                        
                        <input type="hidden" value="{{$borrow->borrowing_id}}" name="id">

                        <div class="form-group">    
                            <label for="book">Book Title</label>
                            <select name="book" id="book" class="form-control" required>
                                <option selected disabled style="color:#eee;">{{$borrow->book_title}}</option>
                                @foreach($book as $b)
                                    <option value="{{$b->book_title}}">{{$b->book_title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option selected value="{{$borrow->borrowing_status}}">{{$borrow->borrowing_status}}</option>
                                <option value="Ready to Take">Ready to Take</option>
                                <option value="Taken">Taken</option>
                                <option value="Returning Book">Returning Book</option>
                            </select>
                        </div>
                        <a href="/borrow" class="btn btn-danger" style="border-radius:50px; outline: none; margin-left:0;">cancel</a>
                        <button type="submit" class="btn btn-primary" style="border-radius:50px; outline: none;">Save</button>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection