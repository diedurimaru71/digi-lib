<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route Landing
Route::get('/', function () {
    return view('welcome');
});

/*
 *-------------------------------------------------------------------------
 * The route used for guest access.
 *-------------------------------------------------------------------------
 *
 * @var string
 */

//Route Home
// Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


/*
 *-------------------------------------------------------------------------
 * The route used for admin access.
 *-------------------------------------------------------------------------
 *
 * @var routes
 */

Route::group(['Middleware' => ['auth', 'checkrole:admin']], function(){

    //Books
    Route::get('/books', 'BooksController@index');
    Route::get('/books/index', 'BooksController@index');
    Route::get('/books/edit/{id}', 'BooksController@edit');
    Route::get('/books/detail/{id}', 'BooksController@detail');
    Route::get('/books/delete/{id}', 'BooksController@delete');
    
    Route::post('/books/search', 'BooksController@search');
    Route::post('/books/addBook', 'BooksController@addBook');
    Route::post('/books/updateBook', 'BooksController@updateBook');

    //borrow
    Route::get('/borrow', 'BorrowController@index');
    Route::get('/borrow/index', 'BorrowController@index');
    Route::get('/borrow/edit/{id}', 'BorrowController@edit');
    Route::get('/borrow/return/{id}', 'BorrowController@return');
    Route::get('/borrow/delete/{id}', 'BorrowController@delete');
    
    Route::post('/borrow/search', 'BorrowController@search');
    Route::post('/borrow/addBorrow', 'BorrowController@addBorrow');
    Route::post('/borrow/updateBorrow', 'BorrowController@updateBorrow');

    //Librarians
    Route::get('/librarian', 'LibrarianController@index');
    Route::get('/librarian/index', 'LibrarianController@index');
    Route::get('/librarian/edit/{id}', 'LibrarianController@edit');
    Route::get('/librarian/delete/{id}', 'LibrarianController@delete');
    
    Route::post('/librarian/search', 'LibrarianController@search');
    Route::post('/librarian/addLibrarian', 'LibrarianController@addLibrarian');
    Route::post('/librarian/updateLibrarian', 'LibrarianController@updateLibrarian');
    
    //Librarians
    Route::get('/student', 'studentController@index');
    Route::get('/student/index', 'studentController@index');
    Route::get('/student/edit/{id}', 'studentController@edit');
    Route::get('/student/detail/{id}', 'StudentController@detail');
    Route::get('/student/delete/{id}', 'studentController@delete');
    
    Route::post('/student/search', 'studentController@search');
    Route::post('/student/addStudent', 'studentController@addStudent');
    Route::post('/student/updateStudent', 'studentController@updateStudent');
});

/*
 *-------------------------------------------------------------------------
 * The route used for librarian access.
 *-------------------------------------------------------------------------
 *
 * @var string
 */

Route::group(['Middleware' => ['auth', 'checkrole:librarian']], function(){
    //Books
    Route::get('/books/detail/{id}', 'BooksController@detail');
    Route::get('/books', 'BooksController@index');
    Route::get('/books/index', 'BooksController@index');
    Route::get('/books/edit/{id}', 'BooksController@edit');
    Route::get('/books/delete/{id}', 'BooksController@delete');

    Route::post('/books/search', 'BooksController@search');
    Route::post('/books/addBook', 'BooksController@addBook');
    Route::post('/books/updateBook', 'BooksController@updateBook');
});

/*
 *-------------------------------------------------------------------------
 * The route used for student access.
 *-------------------------------------------------------------------------
 *
 * @var string
 */

Route::group(['Middleware' => ['auth', 'checkrole:student']], function(){
    
});